from django.db import models
from uuid import uuid4
# Create your models here.
class Customer_details(models.Model):
    id = models.UUIDField(default=uuid4,primary_key=True,unique=True)
    name = models.CharField( max_length=255)
    age = models.IntegerField(max_length=255)
    Gender = models.CharField(max_length=255)
    email = models.EmailField(max_length=255)
    phone_no = models.IntegerField(max_length=255)
    address = models.CharField(max_length=255)
    def __str__(self):
        return self.name

class Order_details(models.Model):
    Invoice_no = models.UUIDField(default=uuid4, unique=True)
    customer = models.ForeignKey(Customer_details, blank=True, on_delete=models.CASCADE)
    item_id = models.UUIDField(default=uuid4, unique=True)
    item_details = models.CharField(max_length=255)
    Quantiy = models.CharField(max_length=255)
    rate = models.CharField(max_length=255)
    def __str__(self):
        return self.item_details

class User_details(models.Model):
    users = models.ForeignKey(Customer_details, blank=True, on_delete=models.CASCADE)
    no_of_item = models.ManyToManyField(Order_details)
    def __str__(self):
        return self.users
