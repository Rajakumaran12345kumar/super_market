from django.apps import AppConfig


class AvsSupermarketConfig(AppConfig):
    name = 'AVS_Supermarket'
