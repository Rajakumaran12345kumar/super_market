from django.contrib import admin
from .models import Customer_details, Order_details, User_details
# Register your models here.
admin.site.register(Customer_details),
admin.site.register(Order_details),
admin.site.register(User_details),
