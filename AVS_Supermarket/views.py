from django.shortcuts import render
from .models import Customer_details
from django.http import HttpResponse, HttpResponseRedirect
# Create your views here.

def Customer_Form(request):
    form = Customer_details
    context = {'form': form}
    if request.method == 'POST':
        form = Customer_details(request.POST)
        names = request.POST['your_name']
        ages = request.POST['your_age']
        genders = request.POST['your_gender']
        contact_email = request.POST['your_email']
        phone_nos = request.POST['your_phone_no']
        addresss = request.POST['your_address']
        customer = Customer_details.objects.create(name=names, age=ages, gender=genders, email=contact_email, phone_no=phone_nos, address=addresss)
        print("Customer")
        customer.save()
        return HttpResponseRedirect('/created/')
    else:
        print("not valid")
    return render(request, 'customer_detail.html', context)

