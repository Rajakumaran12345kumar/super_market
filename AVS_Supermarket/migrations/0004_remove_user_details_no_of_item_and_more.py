# Generated by Django 4.0.2 on 2022-02-14 05:17

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('AVS_Supermarket', '0003_alter_customer_details_phone_no'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='user_details',
            name='no_of_item',
        ),
        migrations.AddField(
            model_name='user_details',
            name='no_of_item',
            field=models.ManyToManyField(to='AVS_Supermarket.Order_details'),
        ),
    ]
